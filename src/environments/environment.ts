// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB-rt7UsyDoilX2PHAdoEaukjuZ4owAyU0",
    authDomain: "exam271.firebaseapp.com",
    projectId: "exam271",
    storageBucket: "exam271.appspot.com",
    messagingSenderId: "49253852436",
    appId: "1:49253852436:web:da51e108bee2300bfd4e01"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
