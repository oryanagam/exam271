export interface Student {
    id:string,
    name: string,
    math: number,
    sat: number,
    payment: boolean,
    category?: string,
}
