import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { StudentsService } from '../students.service';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent implements OnInit {


 
  students$; 
  students:Student[];
  //userId:string; 
  editstate = [];
  addStudentFormOpen = false;
  panelOpenState = false;

  constructor(private studentsService:StudentsService, public authService:AuthService, private db:AngularFirestore
    ) { }

  deleteCustomer(id:string){
    this.studentsService.deleteStudent(id);
  }

  update(student:Student){
    this.studentsService.updateStudent(student.id,student.name,student.math,student.sat,student.payment,student.category);
  }




  add(student:Student){
    this.studentsService.addStudent(student.name,student.math,student.sat,student.payment,student.category)
  }

  /*

  public predict(customer:Customer){
    this.predictService.predict(customer.years,customer.income).subscribe(
      res => {
        console.log(res);
        if(res > 0.5){
          console.log('yes');
          customer.category='will pay';
          customer.ST=1;
          console.log(customer.ST);
        } else {
          console.log('no');
          customer.category='will default';
          customer.ST=1;
        }
          console.log(customer.category);
        
      }
    )
  }*/



  
  ngOnInit(): void {
    this.students$ = this.studentsService.getStudent(); 
        this.students$.subscribe(
          docs =>{
            console.log('init worked');           
            this.students = [];
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              student.id = document.payload.doc.id; 
              this.students.push(student); 
            }
          }
        ) 
  }

}

