import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = "https://paz5ykv5b9.execute-api.us-east-1.amazonaws.com/aleph";

  predict(math: number, sat: number, payment: boolean):Observable<any>{
    let json = {
      "data": 
        {
          "math": math,
          "sat": sat,
          "payment":payment
        }
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        return res.body;       
      })
    );      
  }



  
  constructor(private http: HttpClient) { }
}
