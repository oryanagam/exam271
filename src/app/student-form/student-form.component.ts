import { Student } from './../interfaces/student';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ValidatorFn } from '@angular/forms';
import {FormControl} from '@angular/forms';
import { PredictService } from './../predict.service';
import { Router } from '@angular/router';


@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  students$; 
  students:Student[];


  @Input() name:string;  
  @Input() math:number;
  @Input() sat:number;  
  @Input() payment:boolean;
  @Input() category:string;
  @Input() id:string; 
  @Output() update = new EventEmitter<Student>();
  @Output() closeEdit = new EventEmitter<null>();
  @Output() isError:boolean = false;
  @Output() isErrorsat:boolean = false;
  disableSelect = new FormControl(false);
  @Input() formType:string;

  

  updateParent(){
    let student:Student = {id:this.id, name:this.name, math:this.math, sat:this.sat,payment:this.payment,category:this.category};
    if(this.math>100 || this.math<0){
      this.isError = true;
    }else{
      this.update.emit(student); 
      if(this.formType == "Add student"){
        this.name  = null;
        this.math = null;
        this.sat = null;
        this.payment = null;
        this.category = null;
      }
      this.router.navigate(['/students']); 
      
    }
    if(this.sat>800 || this.sat<0){
      this.isErrorsat = true;
    }else{
      this.update.emit(student);
      if(this.formType == "Add student"){ 
        this.name  = null;
        this.math = null;
        this.sat = null;
        this.payment=null;
        this.category = null;
      }
    
  }
  }


  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  public predict(student:Student){
    this.predictService.predict(this.math,this.sat,this.payment).subscribe(
      res => {
        console.log(res);
        if(res > 0.5){
          console.log('yes');
          student.category='will not drop';
          console.log(student.category);
        } else {
          console.log('no');
          student.category='will drop';
          console.log(student.category);
        }
        
      }
    )
  }

  constructor(private predictService:PredictService, private router:Router) { }

  ngOnInit(): void {
  }

 
}
