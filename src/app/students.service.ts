import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  public getStudent(){
    this.studentCollection = this.db.collection(`/students`); 
    return this.studentCollection.snapshotChanges()      
  } 


  /*save(userId:string,id:string,name:string,years:number,income:number,category:string,ST=2){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income,
        category:category,
        ST:ST
      }
    )
  }*/

  
  deleteStudent(id:string){
    this.db.doc(`/students/${id}`).delete(); 
  } 

  
  addStudent(name:string,math:number,sat:number,payment:boolean,category:string){
    const student = {name:name,math:math,sat:sat,payment:payment,category:category}; 
    this.studentCollection.add(student);
    //this.userCollection.doc(userId).collection('customers').add(customer);
  }


  updateStudent(id:string,name:string,math:number,sat:number,payment:boolean,category:string){
    this.db.doc(`/students/${id}`).update(
      {
        name:name,
        math:math,
        sat:sat,
        payment:payment,
        category:category,


      }
    )
  }
  
  save(id:string,name:string,math:number,sat:number,payment:boolean,category:string,ST=2){
    this.db.doc(`ustudents/${id}`).update(
      {
        name:name,
        math:math,
        sat:sat,
        payment:payment,
        category:category,
        ST:ST
      }
    )
  }

  unsave(id:string,name:string,math:number,sat:number,payment:boolean,category:string,ST=0){
    this.db.doc(`students/${id}`).update(
      {
        name:name,
        math:math,
        sat:sat,
        payment:payment,
        category:category,
        ST:ST
      }
    )
  }




  constructor(private db:AngularFirestore) { }
}
